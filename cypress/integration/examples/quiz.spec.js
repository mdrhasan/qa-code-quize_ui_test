/// <reference types="Cypress" />
import account from  "../../../storage/account.json";

context('Actions', () => {
 
  it('- Verify navigetting to the right url', () => {
   
    cy.visit('/')
    cy.title().should('eq', 'Document')
    cy.get('.sc-bdVaJa > div').should('have.text', 'qa.code-quiz.dev')
    
  })

  it('- Verify login with SomeUser_name credential.', () => {
   
    cy.get('[placeholder="Enter Username"]').type('SomeUser_name')
    cy.get('[placeholder="password"]').type(account.SomeUser_name.password);
    cy.get('.sc-bZQynM').click();
    cy.get('.sc-bdVaJa > div').should('have.text', 'Hello SomeName')
    
  })

  it('- Verify someUser_name dashboard.', () => {
   
    cy.get('.sc-bwzfXH > :nth-child(1) > :nth-child(2)').should('have.text',account.SomeUser_name.name)
    cy.get('.sc-bwzfXH > :nth-child(2) > :nth-child(2)').should('have.text',account.SomeUser_name.favouriteFruit)
    cy.get(':nth-child(3) > :nth-child(2)').should('have.text',account.SomeUser_name.favouriteMovie)
    cy.get(':nth-child(4) > :nth-child(2)').should('have.text',account.SomeUser_name.favouriteNumber)
    
  })

  it('- Verify SomeUser_name logout.', () => {
   
    cy.get('.sc-bxivhb').click();
    cy.get('.sc-bdVaJa > div').should('have.text', 'qa.code-quiz.dev')
    
  })

  it('- Verify login with dummytree credential.', () => {
    
    cy.get('[placeholder="Enter Username"]').type('dummytree')
    cy.get('[placeholder="password"]').type(account.dummytree.password);
    cy.get('.sc-bZQynM').click();
    cy.get('.sc-bdVaJa > div').should('have.text', 'Hello undefined')

  })

  it('- Verify dummytree dashboard.', () => {
    
    cy.get('.sc-bwzfXH > :nth-child(2) > :nth-child(2)').should('have.text',account.dummytree.favouriteFruit)
    cy.get(':nth-child(3) > :nth-child(2)').should('have.text',account.dummytree.favouriteMovie)
    cy.get(':nth-child(4) > :nth-child(2)').should('have.text',account.dummytree.favouriteNumber)

  })

  it('- Verify dummytree logout.', () => {
  
    cy.get('.sc-bxivhb').click();
    cy.get('.sc-bdVaJa > div').should('have.text', 'qa.code-quiz.dev')

  })

 
})
